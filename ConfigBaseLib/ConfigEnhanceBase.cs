﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ConfigBaseLib
{
    public class ConfigEnhanceBase : ConfigArrayBase
    {
        [XmlArray("lable"), XmlArrayItem("item")]
        public List<ConfigItemBase> MyLables { get; set; }

        public string GetLable(string name)
        {
            return base.GetText(name, MyLables);
        }
    }
}
