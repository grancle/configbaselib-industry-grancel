﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using XmlLib;

namespace ConfigBaseLib
{
    public abstract class ConfigHelperBase
    {
        public XElement Root { get; private set; } = null;

        public string RootPath { get; set; } 

        public virtual void Load()
        {
            Root = XmlHelper.Instance.LoadXmlFile(RootPath);
        }

        public virtual void Save()
        {
            XmlHelper.Instance.SaveXml(RootPath, Root);
        }

        private IConfigRoot _myConfig = null;
        //public IConfigRoot MyConfig
        //{
        //    get
        //    {
        //        if (_myConfig == null)
        //        {
        //            _myConfig = GetMyConfig(this.RootPath);
        //        }
        //        return _myConfig;
        //    }
        //}

        protected abstract IConfigRoot GetMyConfig(string path);
    }
}
