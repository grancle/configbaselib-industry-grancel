﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UtilityLib;

namespace ConfigBaseLib
{
    public class ConfigItemBase : IConfigBase
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlText]
        public string MyText { get; set; }

        [XmlIgnore]
        public int MyValue
        {
            get
            {
                return Formulars.Instance.ConvertStrToInt(MyText);
            }
        }

        [XmlIgnore]
        public bool MyBool
        {
            get
            {
                return Formulars.Instance.ConvertStrToBool(MyText);
            }
        }

        //[XmlArray("array"), XmlArrayItem("item")]
        //public List<string> MyArray { get; set; }


        //[XmlAttribute("default", typeof(int))]
        //public int MyDefaultIndex { get; set; }


    }
}
