﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigBaseLib
{
    public static class ConfigArrayExtension
    {
        public static T Get<T>(this List<T> list, string name) where T : IConfigBase
        {
            if (string.IsNullOrEmpty(name) || list == null || list.Count == 0)
            {
                return default(T);
            }
            var result = (from item in list
                          where item.Name == name
                          select item);
            return result.FirstOrDefault();
        }

        public static Tout Get<Tin, Tout>(this List<Tin> list, string name, Func<Tin, Tout> func) where Tin : IConfigBase
        {
            Tin result = list.Get(name);
            if (result == null)
            {
                return default(Tout);
            }
            try
            {
                return func.Invoke(result);
            }
            catch
            {
                return default(Tout);
            }

        }
    }
}
