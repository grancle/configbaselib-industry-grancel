﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UtilityLib;

namespace ConfigBaseLib
{
    public class ConfigArrayBase : IConfigBase
    {
        public T Get<T>(string name, List<T> list) where T : IConfigBase
        {
            if (string.IsNullOrEmpty(name) || list == null || list.Count == 0)
            {
                return default(T);
            }
            var result = (from item in list
                          where item.Name == name
                          select item);
            return result.FirstOrDefault();
        }

        public Tout Get<Tin, Tout>(string name, List<Tin> list, Func<Tin, Tout> func) where Tin : IConfigBase
        {
            Tin result = Get(name, list);
            if (result == null)
            {
                return default(Tout);
            }
            try
            {
                return func.Invoke(result);
            }
            catch
            {
                return default(Tout);
            }
    
        }

        public string GetText<T>(string name, List<T> list) where T : ConfigItemBase
        {
            return Get(name, list, (item) =>
            {
                return item.MyText;
            });
        }

        public int GetValue<T>(string name, List<T> list) where T : ConfigItemBase
        {
            return Get(name, list, (item) =>
            {
                return item.MyValue;
            });
        }

        public bool GetBool<T>(string name, List<T> list) where T : ConfigItemBase
        {
            return Get(name, list, (item) =>
            {
                return item.MyBool;
            });
        }


        public void Set<T>(string name, Action<T> action, List<T> list) where T : IConfigBase
        {
            if (string.IsNullOrEmpty(name) || list == null || list.Count == 0)
            {
                return;
            }
            foreach (var item in list)
            {
                if (item.Name != name)
                {
                    continue;
                }
                action?.Invoke(item);
                break;
            }
        }

        //public List<T2> AddItem<T1, T2>(string name, T1 value, List<T2> list) where T2 : ConfigItemBase
        //{
        //    if(list == null)
        //    {
        //        list = new List<T2>();
        //    }
        //    foreach(var item in list)
        //    {
        //        if(item.Name == name)
        //        {
        //            item.MyText = value.ToString();
        //            return list;
        //        }
        //    }
        //    list.Add(new T2()
        //    {
        //        Name = name,
        //        MyText = value.ToString(),
        //    });
        //    return list;
        //}

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("default")]
        public string MyDefaultIndexStr { get; set; }

        [XmlIgnore]
        public int MyDefaultIndex
        {
            get
            {
                return Formulars.Instance.ConvertStrToInt(MyDefaultIndexStr);
            }
        }

        [XmlArray("array"), XmlArrayItem("item")]
        public List<ConfigItemBase> MyArrayItem { get; set; }

        [XmlIgnore]
        public List<string> MyArrayItemText
        {
            get
            {
                return MyArrayItem.ConvertAll((item) =>
                {
                    return item.MyText;
                });
            }
        }

        [XmlIgnore]
        public List<int> MyArrayItemValue
        {
            get
            {
                return MyArrayItem.ConvertAll((item) =>
                {
                    return item.MyValue;
                });
            }
        }

        public List<T> GetArrayItem<T>(Func<string, T> converter)
        {
            return MyArrayItem.ConvertAll((item) =>
            {
                return converter.Invoke(item.MyText);
            });
        }


        public virtual ConfigArrayBase GetList(string name, List<ConfigArrayBase> list)
        {
            if (string.IsNullOrEmpty(name) || list == null || list.Count == 0)
            {
                return null;
            }
            var result = (from item in list
                          where item.Name == name
                          select item);
            return result.FirstOrDefault();
        }

        public virtual List<string> GetListText(string name, List<ConfigArrayBase> list)
        {
            var result = GetList(name, list);
            if(result == null)
            {
                return null;
            }
            return result.MyArrayItemText;
        }

        public virtual List<int> GetListInt(string name, List<ConfigArrayBase> list)
        {
            var result = GetList(name, list);
            if (result == null)
            {
                return null;
            }
            return result.MyArrayItemValue;
        }


        public List<T> GetListObj<T>(string name, List<ConfigArrayBase> list, Func<string, T> converter)
        {
            var result = GetList(name, list);
            if (result == null)
            {
                return null;
            }
            return result.GetArrayItem<T>(converter);
        }

        //protected virtual List<T> GetArrayImpl<T>(string name, List<ConfigItemBase> list, Func<string, T> converter)
        //{
        //    List<string> result = GetArray(name, list);
        //    return result.ConvertAll((item) =>
        //    {
        //        return converter.Invoke(item);
        //    });
        //}
    }
}
