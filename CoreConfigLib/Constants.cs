﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreConfigLib
{
    public class Constants
    {
        public const string PATH_CORE_CONFIG_SUFFIX = @"\config\config.core.xml";

        public const string PATH_PORT_CONFIG_SUFFIX = @"\config\config.port.xml";
    }
}
