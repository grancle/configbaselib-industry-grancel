﻿using ConfigBaseLib;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UtilityLib;

namespace CoreConfigLib
{
    [XmlRoot("port")]
    public class PortConfigRoot : ConfigArrayBase
    {
        [XmlArray("com"), XmlArrayItem("item")]
        public List<PortConfigConnectOption> MyComOptions { get; set; }

        public string GetComOption(string name)
        {
            return GetText(name, MyComOptions);
        }

        public int GetComOptionInt(string name)
        {
            return GetValue(name, MyComOptions);
        }

        //public Parity GetComOptionParity(string name)
        //{
        //    return Get(name, MyComOptions, (item)=>
        //    {
        //        return PortSettingHelper.Instance.ConvertStrToParity(item?.MyText);
        //    });
        //}

        //public Parity GetComOptionParity(string name)
        //{
        //    return Get(name, MyComOptions, (item) =>
        //    {
        //        return PortSettingHelper.Instance.ConvertStrToParity(item?.MyText);
        //    });
        //}

        public void SetComOption(string name, object value)
        {
            if(MyComOptions == null)
            {
                MyComOptions = new List<PortConfigConnectOption>();
            }

            MyComOptions.Update<PortConfigConnectOption>((item) =>
            {
                item.MyText = value.ToString();
            }, (item) =>
            {
                return item.Name == name;
            }, new PortConfigConnectOption() { 
                Name = name,
                MyText = value.ToString(),
            });
        }

        [XmlIgnore]
        public string name_port //=> GetComOption("port");
        {
            get
            {
                return GetComOption("port");
            }
            set
            {
                SetComOption("port", value);
            }
        }

        [XmlIgnore]
        public int baudrate //=> GetComOptionInt("baudrate");
        {
            get
            {
                return GetComOptionInt("baudrate");
            }
            set
            {
                SetComOption("baudrate", value);
            }
        }

        [XmlIgnore]
        protected string parity_str => GetComOption("parity");

        [XmlIgnore]
        public Parity parity //=> PortSettingHelper.Instance.ConvertStrToParity(parity_str);
        {
            get
            {
                return PortSettingHelper.Instance.ConvertStrToParity(parity_str);
            }
            set
            {
                SetComOption("parity", value);
            }
        }


        [XmlIgnore]
        protected string dataBits_str => GetComOption("dataBits");

        [XmlIgnore]
        public int dataBits //=> Formulars.Instance.ConvertStrToInt(dataBits_str);
        {
            get
            {
                return Formulars.Instance.ConvertStrToInt(dataBits_str);
            }
            set
            {
                SetComOption("dataBits", value);
            }
        }

        [XmlIgnore]
        protected string stopBits_str => GetComOption("stopBits");

        [XmlIgnore]
        public StopBits stopBits //=> PortSettingHelper.Instance.ConvertStrToStopBits(stopBits_str);
        {
            get
            {
                return PortSettingHelper.Instance.ConvertStrToStopBits(stopBits_str);
            }
            set
            {
                SetComOption("stopBits", value);
            }
        }

        [XmlIgnore]
        protected string handshake_str => GetComOption("handshake");

        [XmlIgnore]
        public Handshake handshake //=> PortSettingHelper.Instance.ConvertStrToHandshake(handshake_str);
        {
            get
            {
                return PortSettingHelper.Instance.ConvertStrToHandshake(handshake_str);
            }
            set
            {
                SetComOption("handshake", value);
            }
        }

        [XmlIgnore]
        public bool DtrEnable //=> GetBool("dtr", MyComOptions);
        {
            get
            {
                return GetBool("dtr", MyComOptions);
            }
            set
            {
                SetComOption("dtr", value);
            }
        }

    }
}
