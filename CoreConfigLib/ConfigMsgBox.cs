﻿using ConfigBaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CoreConfigLib
{
    public class ConfigMsgBox : ConfigEnhanceBase
    {
        [XmlIgnore]
        public string LableYes => GetLable("yes");

        [XmlIgnore]
        public string LableNo => GetLable("no");

        [XmlIgnore]
        public string LableOk => GetLable("ok");

        [XmlIgnore]
        public string LableCancle => GetLable("cancle");

        [XmlIgnore]
        public string LableQuit => GetLable("quit");
    }
}
