﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using XmlLib;

namespace CoreConfigLib
{
    public class PortConfigHelper
    {
        public static PortConfigHelper Instance { get; } = new PortConfigHelper();

        public XElement Root { get; private set; } = null;

        public string RootPath { get; set; } = Environment.CurrentDirectory + Constants.PATH_PORT_CONFIG_SUFFIX;

        public void Load()
        {
            Root = XmlHelper.Instance.LoadXmlFile(RootPath);
        }

        public void Save()
        {
            XmlHelper.Instance.SaveXml(RootPath, Root);
        }

        private PortConfigRoot _myConfig = null;
        public PortConfigRoot MyConfig
        {
            get
            {
                if (_myConfig == null)
                {
                    _myConfig = XmlHelper.Instance.Deserialize<PortConfigRoot>(RootPath);
                }
                return _myConfig;
            }
            set
            {
                XmlHelper.Instance.Serialize<PortConfigRoot>(value, RootPath);
            }
        }
    }
}
