﻿using LoggerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using XmlLib;

namespace CoreConfigLib
{
    public class CoreConfigHelper
    {
        public static CoreConfigHelper Instance { get; } = new CoreConfigHelper();

        public XElement Root { get; private set; } = null;

        public string RootPath { get; set; } = PathHelper.GetCurrDir() + Constants.PATH_CORE_CONFIG_SUFFIX;

        public void Load()
        {
            Root = XmlHelper.Instance.LoadXmlFile(RootPath);
        }

        public void Save()
        {
            XmlHelper.Instance.SaveXml(RootPath, Root);
        }

        private ConfigRoot _myConfig = null;
        public ConfigRoot MyConfig
        {
            get
            {
                if (_myConfig == null)
                {
                    _myConfig = XmlHelper.Instance.Deserialize<ConfigRoot>(RootPath);
                }
                return _myConfig;
            }
        }
    }
}
