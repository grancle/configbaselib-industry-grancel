﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CoreConfigLib
{
    public class ConfigSql
    {
        [XmlElement]
        public int SQL_CONNECT_WAIT_MILLISECS { get; set; }

        [XmlElement]
        public int SQL_RETRY_MAX_TIMES { get; set; }

        [XmlElement]
        public int SQL_ERROR_RETURN_VALUE { get; set; }
    }
}
