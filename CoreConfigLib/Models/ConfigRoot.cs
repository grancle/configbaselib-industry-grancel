﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CoreConfigLib
{
    [XmlRoot("core")]
    public class ConfigRoot
    {
        [XmlElement("error_msg")]
        public ConfigErrorMsg ErrMsg { get; set; }

        [XmlElement("com")]
        public ConfigCom MyCom { get; set; }

        [XmlElement("sql")]
        public ConfigSql MySql { get; set; }

        [XmlElement("msgbox")]
        public ConfigMsgBox MyMsgBox { get; set; }
    }
}
