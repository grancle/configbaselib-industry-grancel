﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CoreConfigLib
{
    public class ConfigErrorMsgSql
    {
        [XmlElement]
        public string connect_error { get; set; }

        [XmlElement]
        public string excute_error { get; set; }
    }
}
