﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CoreConfigLib
{
    public class ConfigErrorMsg
    {
        [XmlElement("com")]
        public ConfigErrorMsgCom MyCom { get; set; }

        [XmlElement("sql")]
        public ConfigErrorMsgSql MySql { get; set; }
    }
}
