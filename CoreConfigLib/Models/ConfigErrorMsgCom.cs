﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CoreConfigLib
{
    public class ConfigErrorMsgCom
    {
        [XmlElement]
        public string try_to_connect { get; set; }

        [XmlElement]
        public string connect_error { get; set; }

        [XmlElement]
        public string connect_error_2 { get; set; }

        [XmlElement]
        public string connect_ok { get; set; }

        [XmlElement]
        public string connect_ok_2 { get; set; }

        [XmlElement]
        public string connect_disposed { get; set; }

        [XmlElement]
        public string waiting_to_reconnect { get; set; }

        [XmlElement]
        public string receive_data_error { get; set; }

        [XmlElement]
        public string send_binary_error { get; set; }

        [XmlElement]
        public string send_byte_error { get; set; }

        [XmlElement]
        public string get_available_ports_error { get; set; }

        [XmlElement]
        public string service_error { get; set; }

        [XmlElement]
        public string service_start { get; set; }

        [XmlElement]
        public string service_end { get; set; }

        [XmlElement]
        public string service_create_connect_error { get; set; }
    }
}
