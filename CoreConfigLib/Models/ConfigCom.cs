﻿using ConfigBaseLib;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UtilityLib;

namespace CoreConfigLib
{
    public class ConfigCom : ConfigArrayBase
    {
        [XmlArray("constants"), XmlArrayItem("item")]
        public List<ConfigItemBase> MyConstants { get; set; }

        public string GetConstantText(string name)
        {
            return GetText(name, MyConstants);
        }

        public int GetConstantValue(string name)
        {
            return GetValue(name, MyConstants);
        }

        public bool GetConstantBool(string name)
        {
            return GetBool(name, MyConstants);
        }

        [XmlIgnore]
        public string NAMEID_COM_INTERFACE_PREFIX => GetConstantText("NAMEID_COM_INTERFACE_PREFIX");

        [XmlIgnore]
        public string NAMEID_COM_POOL_SUFFIX => GetConstantText("NAMEID_COM_POOL_SUFFIX");

        [XmlIgnore]
        public int PIPE_WAIT_MILLISECS => GetConstantValue("PIPE_WAIT_MILLISECS");

        [XmlIgnore]
        public int PIPE_CONNECT_WAIT_MILLISECS => GetConstantValue("PIPE_CONNECT_WAIT_MILLISECS");

        [XmlIgnore]
        public int PIPE_RETRY_MAX_TIMES => GetConstantValue("PIPE_RETRY_MAX_TIMES");

        [XmlIgnore]
        public int PIPE_BUFFER_SIZE => GetConstantValue("PIPE_BUFFER_SIZE");

        [XmlIgnore]
        public int PIPE_BUFFER_SIZE_EACH_READ => GetConstantValue("PIPE_BUFFER_SIZE_EACH_READ");

        [XmlIgnore]
        public int COM_CONNECT_WAIT_MILLISECS => GetConstantValue("COM_CONNECT_WAIT_MILLISECS");

        [XmlIgnore]
        public int COM_READ_WAIT_MILLISECS => GetConstantValue("COM_READ_WAIT_MILLISECS");

        [XmlIgnore]
        public int COM_RETRY_MAX_TIMES => GetConstantValue("COM_RETRY_MAX_TIMES");

        [XmlArray("lable"), XmlArrayItem("item")]
        public List<ConfigItemBase> MyLables { get; set; }

        public string GetLable(string name)
        {
            return GetText(name, MyLables);
        }

        [XmlIgnore]
        public string LableGoBack => GetLable("goback");
        [XmlIgnore]
        public string LableConnect => GetLable("connect");
        [XmlIgnore]
        public string LableMoreSetting => GetLable("more");
        [XmlIgnore]
        public string LablePort => GetLable("port");
        [XmlIgnore]
        public string LableBaudrate => GetLable("baudrate");
        [XmlIgnore]
        public string LableDataBit => GetLable("dataBits");
        [XmlIgnore]
        public string LableParity => GetLable("parity");

        [XmlIgnore]
        public string LableStopBits => GetLable("stopBits");
        [XmlIgnore]
        public string LableHandshake => GetLable("handshake");
        [XmlIgnore]
        public string LableDtrEnable => GetLable("dtr");

        [XmlArray("box_list"), XmlArrayItem("list")]
        public List<ConfigArrayBase> MyBoxListArray { get; set; }

        public List<T> GetBoxList<T>(string name, Func<string, T> converter)
        {
            return base.GetListObj<T>(name, MyBoxListArray, converter);
        }

        public List<string> GetBoxList(string name)
        {
            return base.GetListText(name, MyBoxListArray);
        }

        public List<int> GetBoxListToInt(string name)
        {
            return base.GetListInt(name, MyBoxListArray);
        }

        public List<Parity> GetBoxListToParity(string name)
        {
            return GetListObj(name, MyBoxListArray, (item) =>
            {
                return PortSettingHelper.Instance.ConvertStrToParity(item);
            });
        }

        public List<StopBits> GetBoxListToStopbits(string name)
        {
            return GetListObj(name, MyBoxListArray, (item) =>
            {
                return PortSettingHelper.Instance.ConvertStrToStopBits(item);
            });
        }

        public List<Handshake> GetBoxListToHandshake(string name)
        {
            return GetListObj(name, MyBoxListArray, (item) =>
            {
                return PortSettingHelper.Instance.ConvertStrToHandshake(item);
            });
        }

        [XmlIgnore]
        public List<string> BOXLIST_NAME_PORTS => GetBoxList("port");

        [XmlIgnore]
        public List<int> BOXLIST_BAUDRATE => GetBoxListToInt("baudrate");

        [XmlIgnore]
        public List<int> BOXLIST_DATABITS => GetBoxListToInt("dataBits");

        [XmlIgnore]
        public List<Parity> BOXLIST_PARITY => GetBoxListToParity("parity");

        [XmlIgnore]
        public List<StopBits> BOXLIST_STOPBITS => GetBoxListToStopbits("stopBits");

        [XmlIgnore]
        public List<Handshake> BOXLIST_HANDSHAKE => GetBoxListToHandshake("handshake");
    }
}
